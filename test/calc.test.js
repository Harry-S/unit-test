import { assert, expect, should } from "chai";
import { describe } from "mocha";
import calc from "../src/calc.js";

describe("Calc.js,", () => {
    let myVar;
    before(() => {
        myVar = 0;
    });
    beforeEach(() => myVar++);
    after(() => console.log(`myVar: ${myVar}`));
    it("Can add numbers", () => {
        expect(calc.add(2, 2)).to.equal(4)
    });
    it("Can substract numbers", () => {
        assert(calc.substract(5, 1) == 4);
        assert(calc.substract(3, 2) == 1);
        // expect(calc.substract(5, 1)).to.equal(4);
    });
    it("Can multiply numbers", () => {
        should().exist(calc.multiply)
        expect(calc.multiply(3, 3)).to.equal(9)
    });
    it("Can divide numbers", () => {
        expect(calc.divide(2, 4)).to.deep.equal(0.5);
        expect(calc.divide(6, 2)).to.equal(3);

    });
    it("0 division throws an error",() => {
        const err_msg = "0 division not allowed";
        expect(() => calc.divide(1, 0))
            .to
            .throw(err_msg)
    });
});