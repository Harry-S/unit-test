/**
 * Adds two numbers together.
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 */
const add = (a, b) => a + b;
/**
 * Substaracts two numbers.
 * @param {number} minuend 
 * @param {number} subtrahend 
 * @returns {number}
 */
const substract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};
/**
 * Multiplies two numbers.
 * @param {number} minuend 
 * @param {number} subtrahend 
 * @returns {number}
 */
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};
/**
 * Divides two numbers. 
 * @param {number} dividend 
 * @param {number} divisor
 * @returns {number}
 * @throws {Error} 0 division not allowed 
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed")
    const fraction = dividend / divisor;
    return fraction;

};

export default { add, substract, multiply, divide }
